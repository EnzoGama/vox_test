FROM php:8.2.0beta2-fpm-alpine3.15

WORKDIR /var/www

ARG user
ARG uid

ENV build_deps \
        autoconf \
        libzip-dev \
        curl-dev \
        oniguruma-dev \
        libpng-dev \
        libxml2-dev \
        libpq-dev \
        zlib-dev

ENV persistent_deps \
        build-base \
        git \
        unzip \
        curl \
        g++ \
        gcc \
        make \
        rsync \
        openssl \
        acl \
        openrc \
        bash \
        libzip \
        zlib \
        postgresql-dev

RUN apk upgrade --update-cache --available && apk update && \
     apk add --no-cache --virtual .build-dependencies $build_deps

RUN echo 'upload_max_filesize = 20M' >> /usr/local/etc/php/conf.d/docker-php-maxexectime.ini;
RUN echo 'post_max_size = 30M' >> /usr/local/etc/php/conf.d/docker-php-uploads.ini;

RUN apk add --update --no-cache --virtual .persistent-dependencies $persistent_deps && \
     curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
     docker-php-ext-install pdo_pgsql

RUN adduser -D -G www-data -G root -u $uid -h /home/$user $user && \
    mkdir -p /home/$user/.composer

COPY . .

USER $user
