# Vox Test

## Introdução
Projeto de teste da empresa vox que consiste em dois cruds, um de companies(empresas) e outro de partners(sócios)

## Tecnologias Utilizadas
- Symfony
- PHP
- PostgreSQL

## Instalação
1. Clone o repositório: `git clone https://gitlab.com/EnzoGama/vox_test.git`
2. Entre no diretorio do projeto `cd vox_test`
3. Instale as dependências: `composer install`
4. Configure o banco de dados no arquivo `.env`
5. Migre o banco de dados: `php bin/console doctrine:migrations:migrate`
6. Entre na pasta public `cd public`
7. Execute o servidor local: `php -S localhost:8000`

## Acesso ao Projeto
URL Local: `http://localhost:8000`

## Principais rotas de acesso 
URL para empresas : `http://localhost:8000/company`

URL para sócios : `http://localhost:8000/partners`
